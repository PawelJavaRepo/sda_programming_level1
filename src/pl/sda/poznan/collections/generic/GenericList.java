package pl.sda.poznan.collections.generic;

import pl.sda.poznan.collections.Collection;

public interface GenericList<E> extends Collection<E> {
        int size();

        boolean isEmpty();

        boolean contains(E element);

        boolean add(E element);

        void add(int index, E element);

        boolean remove(E element);

        E remove(int index);

        void clear();

        E get(int index);

        E removeFromEnd();

        int indexOf(E element);
}


