package pl.sda.poznan.collections.LinkedList;

import pl.sda.poznan.collections.generic.GenericList;


/*
* Implementacja listy jednokierunkowej z dowiazaniami
* W tej implementacji mamy wskazanie na glowe listy
* Ostatni element listy ma wskazanie next ustawione na null - po tym poznajemy koniec listy
*
* @param <E>
*     */
public class SingleLinkedList<E> implements GenericList<E> {

    private Node<E> head;
    private int size;

    public SingleLinkedList() {
        this.size = 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(E element) {
        return indexOf(element) >= 0;
    }

    @Override
    public boolean add(E element) {
        size++;
        //1.    lista jest pusta (if)
        //2.    lista juz ma elementy
        if (head == null) { // obslugujemy przypadek pustej listy - wstawiamy pierwszy element
            head = new Node<E>(element);
        } else {
            insertDataAtBeggining(element);
        }
        return false;
    }

    @Override
    public E remove() {
        return null;
    }

    private void insertDataAtBeggining(E element) {
        Node<E> newNode = new Node<>(element);
        newNode.setNext(head);
        head = newNode;
    }

    private void insertDataAtEnd(E element) {
        Node<E> helper = head;
        while (helper.getNext() != null) {
            helper = helper.getNext();
        }
        Node<E> newNode = new Node<>(element);
        helper.setNext(newNode);
    }

    @Override
    public void add(int index, E element) {
        Node<E> current = getNodeByIndex(index);
        if(current == head) {
            insertDataAtBeggining(element);
            return;
        }
        Node<E> prev = getNodeByIndex(index - 1);
        Node<E> newNode = new Node<>(element);
        prev.setNext(newNode);
        newNode.setNext(current);
        size++;
    }

    private Node<E> getNodeByIndex(int index) {
        Node<E> helper = head;
        int i = 0;
        while (i != index) {
            helper = helper.getNext();
            i++;
        }
        return head;
    }

    @Override
    public boolean remove(E element) {

        if (!contains(element)) {
            return false;
        }
        Node<E> helper = head;
        Node<E> prev = null;
        while (helper.getNext() != null) {
            if (helper.getValue().equals(element)) {
                break;
            }
            prev = helper;
            helper = helper.getNext();
        }
        // Jezeli poprzednik wynosi null -> oznacza tp, ze mamy skasowac glowe
        //Wiec referencyjnie head przestawiamy o jeden element dalej
        // W przeciwnym przypadku
        if (prev == null) {
            head = helper.getNext();
        } else {
            prev.setNext(helper.getNext());
        }
        helper = null;
        size--;
        return true;
    }


    @Override
    public E remove(int index) {
        E element = this.get(index);
        this.remove(element);
        return element;
    }

    @Override
    public void clear() {
        head = null;
        size = 0;
    }


    @Override
    public E get(int index) {
        int i = 0;
        Node<E> helper = head;
        while (i != index) {
            helper = helper.getNext();
            i++;
        }
        return helper.getValue();
    }

    @Override
    public E removeFromEnd() {
        return remove(size - 1);
    }

    @Override
    public int indexOf(E element) {
        Node<E> helper = head;
        int index = 0;
        while (helper.getNext() != null) {
            index++;
            if (helper.getValue().equals(element)) {
                return index;
            }
            helper = helper.getNext();
            index++;
        }
        return -1;
    }

    public void print() {
        Node<E> helper = head;

        while (helper.getNext() != null) {
            System.out.println(helper.getValue().toString());
            helper = helper.getNext();
        }
        System.out.println(helper.getValue().toString());
    }
}
