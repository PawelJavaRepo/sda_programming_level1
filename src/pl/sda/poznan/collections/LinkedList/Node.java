package pl.sda.poznan.collections.LinkedList;

/*
* klasa reprezentujaca jeden wezel w liscie
* kazdy wezel listy (jednokierunkowej)
* sklada sie z pola danych i wskazania na nastepny element
* */

public class Node<E> {

    // pole przechowujace dane - typ generyczny
    private E value;
    // pole przechowujace referencje (wskazanie do kolejnego wezla)
    private Node<E> next;

    public Node<E> getNext() {
        return next;
    }

    public void setNext(Node<E> next) {
        this.next = next;
    }

    public Node(E value) {
        this.value = value;
    }

    public E getValue() {
        return value;
    }

    public void setValue(E value) {
        this.value = value;
    }

    public Node(E value, Node<E> next) {
        this.value = value;
        this.next = next;
    }
}
