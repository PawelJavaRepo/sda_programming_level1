package pl.sda.poznan.collections.doublelinked;

    public class Node<E> {
        private E data;
        private Node<E> prev;
        private Node<E> next;


        public E getData() {
            return data;
        }

        public void setData(E data) {
            this.data = data;
        }

        public Node (E data){
            this.data = data;
        }

        public Node<E> getPrev(){
            return this.prev;
        }

        public void setPrev(Node<E> prev){
            this.prev = prev;
        }

        public Node<E> getNext() {
            return next;
        }

        public void setNext(Node<E> next) {
            this.next = next;
        }
    }
