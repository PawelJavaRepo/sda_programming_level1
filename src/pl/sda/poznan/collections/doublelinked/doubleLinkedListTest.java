package pl.sda.poznan.collections.doublelinked;

import org.junit.Test;
import pl.sda.poznan.collections.generic.GenericList;

import static org.junit.Assert.*;

public class doubleLinkedListTest {

    GenericList<String> list = new doubleLinkedList<>();

    @Test
    public void shouldAddFirstElement() {
        GenericList<String> list = new doubleLinkedList<>();
        list.add("Pierwszy");
        assertEquals(1, list.size());
    }

    @Test
    public void shouldAddManyElements() {
        list.add("Pierwszy");
        list.add("Drugi");
        list.add("Trzeci");

        assertEquals(3, list.size());
    }

    @Test
    public void shouldGetIndextOfElement() {
        list.add("Pierwszy");
        list.add("Drugi");
        list.add("Trzeci");
        list.add("Czwarty");
        list.add("Piaty");

        assertEquals(0, list.indexOf("Pierwszy"));
        assertEquals(0, list.indexOf("Drugi"));
        assertEquals(0, list.indexOf("Trzeci"));
        assertEquals(0, list.indexOf("Czwarty"));
        assertEquals(0, list.indexOf("Piaty"));
    }

    @Test
    public void shouldRemoveFromEnd() {
        list.add("Pierwszy");
        list.add("Drugi");
        list.add("Trzeci");
        list.add("Czwarty");
        String s = list.removeFromEnd();
        assertEquals(2, list.size());
        assertEquals(s, "Trzeci");
    }

   /* @Test
    public void shouldRemoveFromTheBeginning(){
        doubleLinkedList<String> linkedList = new doubleLinkedList<>();

    }*/

    @Test
    public void shouldReturnFalseWhenElementIsNotPresent() {
        list.add("1");
        list.add("2");

        boolean result = list.remove("zero");
        assertEquals(false, result);
    }

    @Test
    public void shouldRemoveElement(){
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("5");


    }

}